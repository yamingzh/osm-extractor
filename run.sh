#!/bin/bash
OSMXJAR=$(find . -maxdepth 2 -iname 'osm-extractor-*.jar' | head -1)
beast --packages org.openstreetmap.osmosis:osmosis-pbf2:0.48.3 $OSMXJAR $@
