/*
 * Copyright 2021 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import org.openstreetmap.osmosis.core.domain.v0_6.Entity;
import org.openstreetmap.osmosis.core.store.StoreClassRegister;
import org.openstreetmap.osmosis.core.store.StoreReader;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class EntitySerializer extends Serializer<org.openstreetmap.osmosis.core.domain.v0_6.Entity> {
  @Override
  public void write(Kryo kryo, Output output, Entity object) {
    object.store(new KryoStoreWriter(output), new SimpleStoreClassRegister());
  }

  @Override
  public Entity read(Kryo kryo, Input input, Class<Entity> type) {
    try {
      Constructor<Entity> constructor = type.getConstructor(StoreReader.class, StoreClassRegister.class);
      return constructor.newInstance(new KryoStoreReader(input), new SimpleStoreClassRegister());
    } catch (InstantiationException e) {
      throw new RuntimeException(e);
    } catch (IllegalAccessException e) {
      throw new RuntimeException(e);
    } catch (NoSuchMethodException e) {
      throw new RuntimeException(e);
    } catch (InvocationTargetException e) {
      throw new RuntimeException(e);
    }
  }
}
