package edu.ucr.cs.bdlab;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import edu.ucr.cs.bdlab.beast.geolite.Feature;
import edu.ucr.cs.bdlab.beast.io.GeoJSONFeatureWriter;
import org.apache.spark.sql.Row;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class RowConvert {
    public static String toJson(Row row) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        JsonGenerator jsonGenerator = new JsonFactory().createGenerator(outputStream);
        GeoJSONFeatureWriter.writeFeature(jsonGenerator, Feature.create(row, null), false);
        jsonGenerator.close();
        outputStream.close();
        return outputStream.toString();
    }
}
