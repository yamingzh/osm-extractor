package edu.ucr.cs.bdlab;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.JobConfigurable;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.openstreetmap.osmosis.core.domain.v0_6.Entity;

public class PbfEntityInputFormat extends FileInputFormat<LongWritable, Entity> implements JobConfigurable {

    @Override
    protected boolean isSplitable(JobContext context, Path filename) {
        return true;
    }

    @Override
    public PbfEntityRecordReader createRecordReader(InputSplit split, TaskAttemptContext context) {
        // TODO Auto-generated method stub
        return new PbfEntityRecordReader();
    }

    public void configure(JobConf job) {
        // TODO Auto-generated method stub
    }
}